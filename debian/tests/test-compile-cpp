#!/bin/sh
#
# Compile simple C++ client to validate headers and libraries are
# properly set up.  Not running, as network might not be available
# when testing.

cd $AUTOPKGTEST_TMP

# Example code fetched from <URL: https://github.com/savoirfairelinux/opendht >
cat <<EOF > simpleclient.cpp
#include <opendht.h>
#include <vector>

int main()
{
    dht::DhtRunner node;

    // Launch a dht node on a new thread, using a
    // generated RSA key pair, and listen on port 4222.
    node.run(4222, dht::crypto::generateIdentity(), true);

    // Join the network through any running node,
    // here using a known bootstrap node.
    node.bootstrap("bootstrap.jami.net", "4222");

    // put some data on the dht
    std::vector<uint8_t> some_data(5, 10);
    node.put("unique_key", some_data);

    // put some data on the dht, signed with our generated private key
    node.putSigned("unique_key_42", some_data);

    // get data from the dht
    node.get("other_unique_key", [](const std::vector<std::shared_ptr<dht::Value>>& values) {
        // Callback called when values are found
        for (const auto& value : values)
            std::cout << "Found value: " << *value << std::endl;
        return true; // return false to stop the search
    });

    // wait for dht threads to end
    node.join();
    return 0;
}
EOF
LIBS="$(pkg-config --libs opendht)"
if c++ -o simpleclient simpleclient.cpp $LIBS; then
    echo success: building C++ client succeeded
else
    echo error: building C++ client failed
    exit 1
fi

rm -f simpleclient.cpp simpleclient
