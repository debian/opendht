opendht (3.0.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062842

 -- Benjamin Drung <bdrung@debian.org>  Thu, 29 Feb 2024 12:15:24 +0000

opendht (3.0.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control: Update Maintainer to use my new @debian.org address

 -- Amin Bandali <bandali@debian.org>  Thu, 18 Jan 2024 23:32:40 -0500

opendht (3.0.0-4) unstable; urgency=medium

  * Add opendht-tools binary package, providing binaries for the
    various OpenDHT tools developed as part of the upstream code-base.
    (Closes: #866075, #1054359)

 -- Amin Bandali <bandali@gnu.org>  Sun, 22 Oct 2023 12:00:23 -0400

opendht (3.0.0-3) unstable; urgency=medium

  * debian/copyright: Fix Upstream-Name and update Upstream-Contact
  * debian/copyright: Add myself to copyright holders for debian/*
  * debian/control: Bump debhelper-compat to 13
  * debian/copyright: Update Savoir-faire Linux Inc. copyright years
  * debian/dhtnode.conf: Update bootstrap node address

 -- Amin Bandali <bandali@gnu.org>  Sat, 21 Oct 2023 20:25:23 -0400

opendht (3.0.0-2) unstable; urgency=medium

  [ Graham Inggs ]
  * Mark some symbols optional to avoid FTBFS with LTO enabled

 -- Amin Bandali <bandali@gnu.org>  Sat, 30 Sep 2023 01:04:05 -0400

opendht (3.0.0-1) unstable; urgency=medium

  * New upstream release
  * Rename libopendht2 to libopendht3 and libopendht-c2 to
    libopendht-c3 due to SONAME bump (Closes: #1051972)
    - debian/libopendht-c3.install
    - debian/libopendht-c3.symbols
    - debian/libopendht3.install

  * Upload sponsored by Petter Reinholdtsen.

 -- Amin Bandali <bandali@gnu.org>  Sun, 24 Sep 2023 20:01:05 -0400

opendht (2.6.0.4-1) unstable; urgency=medium

  * New upstream release
  * Disable upstream unit tests that require network access
    - debian/rules

 -- Amin Bandali <bandali@gnu.org>  Mon, 11 Sep 2023 16:39:37 -0400

opendht (2.6.0.1-1) unstable; urgency=medium

  * New upstream release
  * Drop upstreamed patch
    - debian/patches/1010-man-page-formatting.patch

 -- Amin Bandali <bandali@gnu.org>  Sun, 10 Sep 2023 14:58:23 -0400

opendht (2.5.5-1) unstable; urgency=medium

  * New upstream release
  * Add new libcppunit-dev Build-Depends to debian/control and drop
    removed CMake options from debian/rules
  * debian/patches/1010-man-page-formatting.patch: forward upstream.
  * debian/patches/2000-cmake-python-no-build-install.patch: mark as
    'Forwarded: not-needed'.

 -- Amin Bandali <bandali@gnu.org>  Wed, 16 Aug 2023 22:31:43 -0400

opendht (2.4.12-7) unstable; urgency=medium

  [ Petter Reinholdtsen ]
  * Add debian/patches/1010-man-page-formatting.patch by Jakob Haufe
    to address lintian
    warning: macro 'mF' not defined [usr/share/man/man1/dhtnode.1.gz:1].
  * Sponsor upload to unstable.

 -- Amin Bandali <bandali@gnu.org>  Wed, 22 Feb 2023 15:20:06 -0500

opendht (2.4.12-6) experimental; urgency=medium

  [ Petter Reinholdtsen ]
  * debian/libopendht-c2.symbols: Exclude symbols on hurd-i386 and sh4
    too.

  [ Amin Bandali ]
  * debian/control: Taking over the maintenance of the package; many
    thanks, Alexandre!
  * debian/libopendht-c2.symbols: Add three symbols on armel.

  * Upload sponsored by Petter Reinholdtsen.

 -- Amin Bandali <bandali@gnu.org>  Mon, 20 Feb 2023 23:25:50 -0500

opendht (2.4.12-5) unstable; urgency=medium

  [ Amin Bandali ]
  * debian/libopendht-c2.symbols: Add exclusions for various symbols
    across most supported architectures per buildd.debian.org logs.

  [ Petter Reinholdtsen ]
  * debian/rules: Use DEB_BUILD_GNU_TYPE for locating build directory.
  * Sponsor upload to unstable.

 -- Amin Bandali <bandali@gnu.org>  Tue, 07 Feb 2023 16:42:22 -0500

opendht (2.4.12-4) unstable; urgency=medium

  * debian/control: Switch 'python3-all' to 'python3-all-dev:any' to
    hopefully fix builds broken with 'fatal error: Python.h: No such
    file or directory'.  Also tweak a few descriptions.

  * Upload sponsored by Petter Reinholdtsen.

 -- Amin Bandali <bandali@gnu.org>  Tue, 07 Feb 2023 02:21:56 -0500

opendht (2.4.12-3) unstable; urgency=medium

  * Fix build more generally on architectures like ARM EABI (armel)
    and little-endian MIPS (mipsel) that don't have native 64-bit
    atomic operations by linking against libatomic. (Closes: #1030615)
  * Address several lintian warnings:
    - Drop usr/include/opendht/opendht_c.h from libopendht-dev;
      it already belongs in libopendht-c-dev. (Closes: #1030696)
    - debian/control: Add 'Multi-Arch: same' for libopendht2 and
      libopendht-c2.

  * Upload sponsored by Petter Reinholdtsen.

 -- Amin Bandali <bandali@gnu.org>  Tue, 07 Feb 2023 01:27:18 -0500

opendht (2.4.12-2) experimental; urgency=medium

  * Switch from static to shared library.  Upstream has declared their
    API and ABI stable, therefore we now build and provide a shared
    library rather than a static one.
  * Add python3-opendht providing OpenDHT Python 3 bindings
    (Closes: #850051).
  * Add simple debian/tests/test-run-python to validate Python 3
    bindings.
  * Add OpenDHT C shared library and bindings.

  * Upload sponsored by Petter Reinholdtsen.

 -- Amin Bandali <bandali@gnu.org>  Sat, 04 Feb 2023 01:07:30 -0500

opendht (2.4.12-1.2) unstable; urgency=medium

  * Fix build more generally on architectures like ARM EABI (armel)
    and little-endian MIPS (mipsel) that don't have native 64-bit
    atomic operations by linking against libatomic. (Closes: #1030615)

  * Upload sponsored by Petter Reinholdtsen.

 -- Amin Bandali <bandali@gnu.org>  Sun, 05 Feb 2023 13:10:19 -0500

opendht (2.4.12-1.1) unstable; urgency=medium

  * Attempt to fix build on armel.

  * Upload sponsored by Petter Reinholdtsen.

 -- Amin Bandali <bandali@gnu.org>  Sat, 04 Feb 2023 13:02:57 -0500

opendht (2.4.12-1) unstable; urgency=medium

  * New upstream version 2.4.12.
  * debian/copyright: Remove vestigial 'Files-Excluded: src/argon2/*'
    and update copyright years.
  * Update package descriptions per upstream.

  * Upload sponsored by Petter Reinholdtsen.

 -- Amin Bandali <bandali@gnu.org>  Sat, 04 Feb 2023 01:05:20 -0500

opendht (2.4.10-1) unstable; urgency=medium

  * Collab-maint upload with maintainer approval.

  * New upstream version 2.4.10 (Closes: 1016489).
    - Refreshed patches to remove fuzzies.
    - Updated package description to relect new C++17 requirement.
    - Dropped call to dh_dwz during build as it fail on
      debian/dhtnode/usr/bin/dhtnode.
  * Added patch header in pkgconfig-extras.patch.
  * Switched patches to 0000/1000/2000 naming convention.
  * Added build-essential and pkg-config as autopkgtest dependencies.
  * Changed dependency for libmsgpack-dev to prefer new libmsgpack-cxx-dev
    (Closes: #1019113).

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 27 Dec 2022 10:03:41 +0100

opendht (2.3.1-3) unstable; urgency=medium

  * Collab-maint upload with maintainer approval.

  * Updated standards version from 4.5.0 to 4.6.2.  No changes needed.
  * Switched build dependency libncurses5-dev to libncurses-dev.
  * Corrected notation for new uploader.
  * Added pkgconfig-extras.patch with libraries needed to link
    simple C++ client.
  * Added simple autopkgtest C++ build test.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 26 Dec 2022 07:55:50 +0100

opendht (2.3.1-2) unstable; urgency=medium

  * Collab-maint upload with maintainer approval.

  * Acknowledge NMU (Closes: #1026871).
  * Added Amin Bandali as uploader.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 25 Dec 2022 06:42:25 +0100

opendht (2.3.1-1.1) unstable; urgency=medium

  * Non-maintainer upload using salsa Debian team git repo.

  [ Amin Bandali ]
  * d/watch: Tweak opts to use newly-suggested format in the current
    uscan(1) manual for GitHub repositories, helping correctly detect
    new releases again (partly fixes #1016489).

  [ Federico Ceratto ]
  * Configure service sandbox (Closes: #1007163).
  * Bump up Standards-Version from 4.0.0 to 4.5.0.
  * Switch to debhelper-compat 12.

  [ Petter Reinholdtsen ]
  * Switched build and binary dependency for libargon2-0-dev to libargon2-dev
    (Closes: #1005699).

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 22 Dec 2022 22:33:19 +0100

opendht (2.3.1-1) unstable; urgency=medium

  [ Amin Bandali ]
  * d/watch: Update to version=4 and use newly-suggested opts format
    in the current uscan(1) manual for GitHub repositories.  This is
    motivated in part by a change in devscripts-2.21.5 that makes
    uscan die when filenamemangle fails.

  [ Alexandre Viau ]
  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 20 Nov 2021 23:59:14 -0500

opendht (2.1.10-1) unstable; urgency=medium

  * New upstream version. (Closes: 978562)

 -- Alexandre Viau <aviau@debian.org>  Fri, 01 Jan 2021 14:02:19 -0500

opendht (2.1.9.5-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * New upstream version.

  [ Amin Bandali ]
  * Bump debhelper compat level up to 10.

 -- Alexandre Viau <aviau@debian.org>  Tue, 08 Dec 2020 17:56:05 -0500

opendht (2.1.6-1) unstable; urgency=medium

  [ Amin Bandali ]
  * New upstream version.
  * Enable OpenDHT proxy features.

  [ Alexandre Viau ]
  * Rules-Requires-Root: no.

 -- Alexandre Viau <aviau@debian.org>  Sat, 10 Oct 2020 16:35:04 -0400

opendht (2.1.4-1) unstable; urgency=medium

  * New upstream version.
  * dhtnode.conf: remove -v. (Closes: #959226)

 -- Alexandre Viau <aviau@debian.org>  Sat, 15 Aug 2020 12:42:20 -0400

opendht (2.1.1-1) unstable; urgency=medium

  * New upstream version.
  * Depend on librestinio-dev, libasio-dev.
  * Refresh patches.

 -- Alexandre Viau <aviau@debian.org>  Sat, 16 May 2020 13:08:04 -0400

opendht (1.8.1-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 05 Jan 2019 21:57:00 -0500

opendht (1.8.0-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Mon, 10 Dec 2018 20:15:28 -0500

opendht (1.7.4-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 18 Aug 2018 15:51:19 -0400

opendht (1.7.2-1) unstable; urgency=medium

  * New upstream version.
  * Fix maintainer-script-should-not-use-recursive-chown-or-chmod.

 -- Alexandre Viau <aviau@debian.org>  Tue, 26 Jun 2018 18:08:36 -0400

opendht (1.6.0-1) unstable; urgency=medium

  * d/watch: exclude release candidates.
  * d/copyright: fix insecure-copyright-format-uri.

 -- Alexandre Viau <aviau@debian.org>  Tue, 27 Feb 2018 06:11:11 +0000

opendht (1.5.0-3) unstable; urgency=medium

  * Build with msgpack-c v2 API.

 -- Alexandre Viau <aviau@debian.org>  Thu, 01 Feb 2018 17:47:24 +0000

opendht (1.5.0-2) unstable; urgency=medium

  * Move to salsa.debian.org.

 -- Alexandre Viau <aviau@debian.org>  Thu, 28 Dec 2017 17:09:51 -0500

opendht (1.5.0-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Alexandre Viau <aviau@debian.org>  Thu, 23 Nov 2017 19:44:27 -0500

opendht (1.4.1-1) unstable; urgency=medium

  * New upstream snapshot.
  * Use msgpack v1 API.

 -- Alexandre Viau <aviau@debian.org>  Wed, 15 Nov 2017 20:54:42 -0500

opendht (1.3.6-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Alexandre Viau <aviau@debian.org>  Fri, 25 Aug 2017 16:44:04 -0400

opendht (1.3.5-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Alexandre Viau <aviau@debian.org>  Wed, 02 Aug 2017 15:41:36 -0400

opendht (1.3.4-3) unstable; urgency=medium

  * Remove unneeded multiarch.patch.

 -- Alexandre Viau <aviau@debian.org>  Sun, 02 Jul 2017 14:35:50 -0400

opendht (1.3.4-2) unstable; urgency=medium

  * dhtnode.service: restart on-failure

 -- Alexandre Viau <aviau@debian.org>  Sat, 01 Jul 2017 15:20:42 -0400

opendht (1.3.4-1) unstable; urgency=medium

  * New upstream snapshot.
  * dhtnode: include systemd service.

 -- Alexandre Viau <aviau@debian.org>  Sat, 01 Jul 2017 14:51:02 -0400

opendht (1.3.3-2) unstable; urgency=medium

  * Install CMakeFiles. (Closes: #866586)

 -- Alexandre Viau <aviau@debian.org>  Fri, 30 Jun 2017 16:03:24 -0400

opendht (1.3.3-1) unstable; urgency=medium

  * New upstream snapshot. (Closes: #866078)
  * Refresh patches.
  * Remove use-debian-argon2.patch.
  * Remove python bindings mention.
  * Bump Standards-Version to 4.0.0.
  * Build-Depend on pkg-config.

 -- Alexandre Viau <aviau@debian.org>  Tue, 27 Jun 2017 18:42:38 -0400

opendht (1.2.1~dfsg1-8) unstable; urgency=medium

  * dev package: Add library dependencies (Closes: #849561).

 -- Alexandre Viau <aviau@debian.org>  Wed, 28 Dec 2016 17:11:11 -0500

opendht (1.2.1~dfsg1-7) unstable; urgency=medium

  [ Alexandre Viau ]
  * Import Gianfranco's work (Closes: #843788).

  [ Gianfranco Costamagna ]
  * Multiarchify the package.

 -- Alexandre Viau <aviau@debian.org>  Wed, 09 Nov 2016 11:28:54 -0500

opendht (1.2.1~dfsg1-6) unstable; urgency=medium

  * libopendht-dev now breaks+replaces libopendht1 (Closes: #843680)

 -- Alexandre Viau <aviau@debian.org>  Tue, 08 Nov 2016 13:13:44 -0500

opendht (1.2.1~dfsg1-5) unstable; urgency=medium

  * Don't ship libopendht1. Debian policy allows for shipping
    only static libraries since OpenDHT's API is not stable
    enough.

 -- Alexandre Viau <aviau@debian.org>  Mon, 07 Nov 2016 13:14:31 -0500

opendht (1.2.1~dfsg1-4) unstable; urgency=medium

  * Add libopendht.shlibs.

 -- Alexandre Viau <aviau@debian.org>  Sat, 05 Nov 2016 01:00:46 -0400

opendht (1.2.1~dfsg1-3) unstable; urgency=medium

  * Remove pkg-kde-tools dependency.

 -- Alexandre Viau <aviau@debian.org>  Fri, 04 Nov 2016 16:10:56 -0400

opendht (1.2.1~dfsg1-2) unstable; urgency=medium

  * Link against argon2.

 -- Alexandre Viau <aviau@debian.org>  Fri, 04 Nov 2016 03:29:16 -0400

opendht (1.2.1~dfsg1-1) unstable; urgency=medium

  * Remove symbols file.
  * Refresh use-debian-argon2.patch.
  * Remove build_versioned_shared_library.patch.
  * Use manpage from source.

 -- Alexandre Viau <aviau@debian.org>  Wed, 29 Jun 2016 15:52:22 +0200

opendht (0.6.1~dfsg1-1) experimental; urgency=medium

  * Initial release. (Closes: #809362)

 -- Alexandre Viau <aviau@debian.org>  Mon, 27 Jun 2016 13:09:11 +0200
